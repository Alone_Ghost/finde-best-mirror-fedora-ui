#include "dialog.h"
#include "ui_dialog.h"
#define ROOT_ETC_YUM_PATH "/etc/yum.repos.d/"
#define BYTE_DOWNLOAD_ 500000

#define ROOT_ETC_RPMFUSION_INSTALLATION_PATH "/etc/yum.repos.d/DownloadRpmfusion/"

#include <QtXml>

Dialog::Dialog(QWidget *parent) : QDialog(parent),
  ui(new Ui::Dialog)
{

  ui->setupUi(this);
  ui->addedRpmfusionFreeButton->setDisabled(true);
  ui->addedRpmFusionNonFreeButton->setDisabled(true);
  ui->addedBothRepoRpmfusionButton->setDisabled(true);

}

Dialog::~Dialog()
{
  delete ui;
}

void Dialog::findInfoSystem()
{

  _releaseever =  productVersion.productVersion();
  _baseArch = currentCpuArchitecture.currentCpuArchitecture();
}

void Dialog::createPatchForIni()
{
  QDir dir(ROOT_ETC_YUM_PATH "IniFiles");

  if (!dir.exists())
    {
      dir.mkpath(ROOT_ETC_YUM_PATH "IniFiles");
    }

  QSettings settings(ROOT_ETC_YUM_PATH "IniFiles/alghoritmRepo.ini" , QSettings::IniFormat);

  settings.beginGroup("fedora");
  settings.setValue("alghorithmFedoraMain" , "$link/releases/$releasever/Everything/$basearch/os/");
  settings.endGroup();

  settings.beginGroup("fedora-update.repo");
  settings.setValue("alghorithmFedoraUpdateMin" ,"$link/updates/$releasever/Everything/$basearch/");
  settings.endGroup();

  settings.beginGroup("rpmfusion-free.repo");
  settings.setValue("alghorithmRpmFusion" ,"$link/free/fedora/releases/$releasever/Everything/$basearch/os/");
  settings.endGroup();

  settings.beginGroup("rpmfusion-free-updates.repo");
  settings.setValue("alghorithmRpmfusionFreeUpdate" ,"$link/free/fedora/updates/$releasever/$basearch/");
  settings.endGroup();

  settings.beginGroup("rpmfusion-nonfree");
  settings.setValue("alghorithmRpmfusionNonFree" ,"$link/nonfree/fedora/releases/$releasever/Everything/$basearch/os/");
  settings.endGroup();

  settings.beginGroup("rpmfusion-nonfree-updates");
  settings.setValue("alghorithmRpmfusionNonFreeUpdate" ,"$link/nonfree/fedora/update/$releasever/$basearch/");
  settings.endGroup();

  QSettings settingsDynamicLink(ROOT_ETC_YUM_PATH "IniFiles/alghoritmDynamicLink.ini" , QSettings::IniFormat);
  settingsDynamicLink.beginGroup("dynamicLink");
  settingsDynamicLink.setValue("alghorithmDynamicLink" ,"$link/releases/$releasever/Everything/$basearch/os/Packages/a/");
  settingsDynamicLink.endGroup();

  QSettings settingInstallRpmFusion(ROOT_ETC_YUM_PATH "IniFiles/alghoritmInstallRpmfusion.ini" , QSettings::IniFormat);
  settingInstallRpmFusion.beginGroup("RpmFusionFree");
  settingInstallRpmFusion.setValue("alghorithmDownloadRpmFusionFree" ,"https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$releasever.noarch.rpm");
  settingInstallRpmFusion.endGroup();

  settingInstallRpmFusion.beginGroup("alghorithmDownloadRpmFusionNonFree");
  settingInstallRpmFusion.setValue("alghorithmDownloadRpmFusionNonFree" ,"https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$releasever.noarch.rpm");
  settingInstallRpmFusion.endGroup();
}

void Dialog::regexFindLinksFedora()
{
  _linksFedora = _htmlFile;

  unsigned long positionStartLinkRegexSitFedora = _htmlFile.indexOf("<table>");
  unsigned long positionFinishLinkRegexSitFedora = _htmlFile.indexOf("</table>");
  while ((positionStartLinkRegexSitFedora = regexFedoraLinks.indexIn(_linksFedora, positionStartLinkRegexSitFedora)) <= positionFinishLinkRegexSitFedora)
    {
      _listLinkFedora << regexFedoraLinks.cap();
      if(positionStartLinkRegexSitFedora <= positionFinishLinkRegexSitFedora)
        positionStartLinkRegexSitFedora += regexFedoraLinks.matchedLength();
    }

  QStringList filterList;
  filterList =  _listLinkFedora.filter("epel");

  for (int i =0 ; i < _listLinkFedora.count();i++)
    {
      for (int b = 0 ; b < filterList.count(); b++)
        {
          if(_listLinkFedora.at(i) == filterList.at(b))
            {
              _listLinkFedora.removeAt(i);
            }
        }
    }

  QStringList list = _listLinkFedora;
  list = list.toSet().toList();
  _listLinkFedora = list;
}

void Dialog::on_runButton_clicked()
{
  QUrl adressFedora{"https://admin.fedoraproject.org/mirrormanager/mirrors/Fedora/30/x86_64"};
  downloadWebsitFedora(adressFedora);
}

void Dialog::downloadWebsitFedora(QUrl url )
{
  qDebug() << __FUNCTION__;

  _manager = new QNetworkAccessManager(this);
  connect(_manager , &QNetworkAccessManager::finished , this , &Dialog::findLinksFedora);
  _URL = url;
  _request.setUrl(_URL);
  _manager->get(_request);
}

void Dialog::findLinksFedora(QNetworkReply *findLinksFedora)
{
  qDebug() << __FUNCTION__;

  dataFedora=findLinksFedora->readAll();
  _htmlFile = dataFedora;
  _linksFedora = _htmlFile;

  unsigned long positionStartNameCountryRegexfedora = _htmlFile.indexOf("<table>");
  unsigned long positionFinishNameCountryregexfedora = _htmlFile.indexOf("</table>");

  //    regexFindLinksFedora();

  QString hi;
  int i =50;
  int posSForStartSelection = _linksFedora.indexOf("<tr>");
  int posSFinishSelection = _linksFedora.indexOf("</tr>");

  do
    {
      hi = _linksFedora.section("\n",posSForStartSelection , posSFinishSelection);
      qDebug () << hi;
//       posSFinishSelection  = posSFinishSelection + 1 ;
      _linksFedora.remove(hi);

    }while (i >= 0);


//  while ((positionStartNameCountryRegexfedora = regexExtractionOfSiteNameCountryFedora.indexIn(_htmlFile, positionStartNameCountryRegexfedora)) <= positionFinishNameCountryregexfedora)
//    {
//      _findListOfSiteNameCountryFedora << regexExtractionOfSiteNameCountryFedora.cap();
//      positionStartNameCountryRegexfedora += regexExtractionOfSiteNameCountryFedora.matchedLength();
//    }


  //    QStringList _counterListLinkFedora;
  //    QString stringNameCountry = _findListOfSiteNameCountryFedora.join(",");

  //    unsigned long postionStartFindListNames = 0;
  //    while ((postionStartFindListNames = regexExtractionOfListNameCountryFedora.indexIn(stringNameCountry, postionStartFindListNames)) !=-1)
  //    {
  //        _findListOfListNameCountیادگیریryFedora << regexExtractionOfListNameCountryFedora.cap();
  //        postionStartFindListNames += regexExtractionOfListNameCountryFedora.matchedLength();
  //    }

  //    for (int i = 0; i <_findListOfListNameCountryFedora.length(); i++ )
  _listLinkFedoraConcomitantNameCountry.insert(_findListOfSiteNameCountryFedora , _listLinkFedora);
  for (int i = 0; i <= _findListOfSiteNameCountryFedora.length(); i++ )
    {
      QString takeItemOfNameCountryOflistForTreeAndQmap;
      takeItemOfNameCountryOflistForTreeAndQmap = _findListOfSiteNameCountryFedora.takeFirst();
      QString takeItemLinkOfListForTreeWidget= _listLinkFedora.takeFirst();
      addRootTreeWidget(takeItemOfNameCountryOflistForTreeAndQmap ,takeItemLinkOfListForTreeWidget  , "Fedora");
    }


  //start RpmFusion
  QUrl adressRpmFusion{"https://mirrors.rpmfusion.org/mm/publiclist/RPMFUSION%20free%20Fedora/30/x86_64/"};
  downloadWebsitRpmfusion(adressRpmFusion);
}


void Dialog::downloadWebsitRpmfusion(QUrl url)
{
  _manager = new QNetworkAccessManager(this);

  connect(_manager , &QNetworkAccessManager::finished , this , &Dialog::findLinksRpmFusion);

  _URL = url;
  _request.setUrl(_URL);
  _manager->get(_request);

}
void Dialog::findLinksRpmFusion(QNetworkReply *findLinksRpmfusion)
{
  dataRpmfusion=findLinksRpmfusion->readAll();
  _htmlFileRpmfusion = dataRpmfusion;
  _linksRpmFusion = _htmlFileRpmfusion;

  unsigned long startPositionList = _htmlFileRpmfusion.indexOf("<tr><th>Country</th><th>Site</th><th>Host</th><th>Content</th><th>Bandwidth (Mbits/sec)</th><th>I2</th><th>Comments</th></tr>");
  unsigned long FinishPositionList = _htmlFileRpmfusion.indexOf("</html>");

  unsigned long startPositionListCountry = _htmlFileRpmfusion.indexOf("<tr><th>Country</th><th>Site</th><th>Host</th><th>Content</th><th>Bandwidth (Mbits/sec)</th><th>I2</th><th>Comments</th></tr>");
  unsigned long FinishPositionListCountry = _htmlFileRpmfusion.indexOf("</html>");


  while ((startPositionList = regexRpmFusionLinks.indexIn(_linksRpmFusion, startPositionList)) <= FinishPositionList)
    {
      _listLinksRpmFuison << regexRpmFusionLinks.cap();
      if(startPositionList <= FinishPositionList)
        startPositionList += regexRpmFusionLinks.matchedLength();
      else
        {
          break;
        }
    }

  QStringList list = _listLinksRpmFuison;
  list = list.toSet().toList();
  _listLinksRpmFuison = list;

  while ((startPositionListCountry = regexExtractionOfSiteNameCountryRpmfusion.indexIn(_linksRpmFusion , startPositionListCountry)) <= FinishPositionListCountry )
    {
      _findListOfSiteNameCountryRpmFuisn << regexExtractionOfSiteNameCountryRpmfusion.cap();
      if(startPositionListCountry <= FinishPositionListCountry)
        startPositionListCountry += regexExtractionOfSiteNameCountryRpmfusion.matchedLength();

    }

  //    QString stringNameCountry = _findListOfSiteNameCountryRpmFuisn.join(",");

  //    unsigned long postionStartFindListNames = 0;
  //    while ((postionStartFindListNames = regexExtractionOfListNameCountryRpmfusion.indexIn(stringNameCountry, postionStartFindListNames)) !=-1)
  //    {
  //        _findListOfListNameCountryRpmFusion << regexExtractionOfListNameCountryRpmfusion.cap();
  //        postionStartFindListNames += regexExtractionOfListNameCountryRpmfusion.matchedLength();

  //    }
  int lengjklj = _findListOfSiteNameCountryRpmFuisn.length();

  for (int i = 0; i <=lengjklj; i++ )
    {
      QString takeItemOfNameCountryOflistForTreeAndQmap;
      QStringList forTakeItemOflistLinkForQmap;

      forTakeItemOflistLinkForQmap.insert(i ,_listLinksRpmFuison[i]); //for value QMap
      takeItemOfNameCountryOflistForTreeAndQmap = _findListOfSiteNameCountryRpmFuisn.takeFirst();

      _listLinkRpmfusionConcomitantNameCountry.insert(takeItemOfNameCountryOflistForTreeAndQmap , forTakeItemOflistLinkForQmap);
      QString takeItemLinkOfListForTreeWidget = forTakeItemOflistLinkForQmap.takeFirst();

      addRootTreeWidget(takeItemOfNameCountryOflistForTreeAndQmap ,takeItemLinkOfListForTreeWidget , "RpmFusion");
    }
}



void Dialog::addRootTreeWidget(QString nameTree , QString nameChild , QString forwhat)
{

  if(forwhat == "Fedora")
    {
      QTreeWidgetItem *treeItemMain = new QTreeWidgetItem(ui->showMainRepo);
      treeItemMain->setText(0, nameTree);
      addChildTreeWidget(treeItemMain , nameChild);
    }
  else
    {
      QTreeWidgetItem *treeItemRpmFusion = new QTreeWidgetItem(ui->showMainRepo);
      treeItemRpmFusion->setText(0, nameTree);
      addChildTreeWidget(treeItemRpmFusion , nameChild);
    }
}
void Dialog::addChildTreeWidget(QTreeWidgetItem *parent, QString link )
{
  QTreeWidgetItem *treeItem = new QTreeWidgetItem();
  treeItem->setText(0, link);
  parent->addChild(treeItem);
}

void Dialog::on_exitButton_clicked()
{
  close();
}

void Dialog::on_addedRepostioryRpmFusion_clicked()
{
  ui->addedRepostioryRpmFusion->setEnabled(false);
  ui->addedRpmfusionFreeButton->setDisabled(false);
  ui->addedRpmFusionNonFreeButton->setDisabled(false);
  ui->addedBothRepoRpmfusionButton->setDisabled(false);
}

void Dialog::on_addedRpmfusionFreeButton_clicked()
{
  createLinkForDownloadRpmfusionFree();
}
void Dialog::on_addedBothRepoRpmfusionButton_clicked()
{
  createLinkForDownloadRpmfusionFree();
  createLinkForDownloadRpmfusionNonFree();
}

void Dialog::on_addedRpmFusionNonFreeButton_clicked()
{
  createLinkForDownloadRpmfusionNonFree();
}

void Dialog::createLinkForDownloadRpmfusionFree()
{
  QSettings settings(ROOT_ETC_YUM_PATH "IniFiles/alghoritmInstallRpmfusion.ini", QSettings::IniFormat);

  //Alghoritm Create Download Link Rpmfusion Free
  settings.beginGroup("RpmFusionFree");
  _aIniFileRpmfusionFree = settings.value("alghorithmDownloadRpmFusionFree").toString();
  settings.endGroup();

  _aIniFileRpmfusionFree.replace("$releasever" ,_releaseever);
  downloadRpmfusionFree();

}

void Dialog::createLinkForDownloadRpmfusionNonFree()
{
  QSettings settings(ROOT_ETC_YUM_PATH "IniFiles/alghoritmInstallRpmfusion.ini", QSettings::IniFormat);

  //Alghoritm Create Download Link Rpmfusion Non-Free
  settings.beginGroup("alghorithmDownloadRpmFusionNonFree");
  _aIniFileRpmfusionNonFree = settings.value("alghorithmDownloadRpmFusionNonFree").toString();
  settings.endGroup();

  _aIniFileRpmfusionNonFree.replace("$releasever" ,_releaseever);
  downloadRpmfusionNonFree();

}
void Dialog::downloadRpmfusionFree()
{
  _manager = new QNetworkAccessManager(this);
  connect(_manager , &QNetworkAccessManager::finished , this , &Dialog::writeRpmFusionFree);

  _URL = _aIniFileRpmfusionFree;
  _request.setUrl(_URL);
  _manager->get(_request);

}
void Dialog::writeRpmFusionFree(QNetworkReply *wirteThatFile)
{
  QDir dir(ROOT_ETC_RPMFUSION_INSTALLATION_PATH);
  if(!dir.exists())
    {
      dir.mkpath(ROOT_ETC_YUM_PATH);
    }
  else
    {
      if(wirteThatFile->error())
        {
          qDebug() << "ERROR!";
          qDebug() << wirteThatFile->errorString();
        }
      else
        {

          QFile *file = new QFile(ROOT_ETC_RPMFUSION_INSTALLATION_PATH"RpmFusionFree.rpm");
          if(file->open(QFile::Append))
            {
              file->write(wirteThatFile->readAll());
              file->flush();
              file->close();
            }
        }
    }

  QString adressInstallitionRpmfusionNonFree{"dnf install /etc/yum.repos.d/DownloadRpmfusion/RpmFusionFree.rpm"};
  installRpmfusionNonFree(adressInstallitionRpmfusionNonFree);

}

void Dialog::downloadRpmfusionNonFree()
{
  _manager = new QNetworkAccessManager(this);
  connect(_manager , &QNetworkAccessManager::finished , this , &Dialog::writeRpmFusionFree);

  _URL = _aIniFileRpmfusionNonFree;
  _request.setUrl(_URL);
  _manager->get(_request);

}

void Dialog::writeRpmFusionNonFree(QNetworkReply *wirteThatFile)
{
  QDir dir(ROOT_ETC_RPMFUSION_INSTALLATION_PATH);
  if(!dir.exists())
    {
      dir.mkpath(ROOT_ETC_RPMFUSION_INSTALLATION_PATH);
    }
  else
    {
      if(wirteThatFile->error())
        {
          qDebug() << "ERROR!";
          qDebug() << wirteThatFile->errorString();
        }
      else
        {
          QFile *file = new QFile(ROOT_ETC_RPMFUSION_INSTALLATION_PATH"RpmFusionNonFree.rpm");
          if(file->open(QFile::Append))
            {
              file->write(wirteThatFile->readAll());
              file->flush();
              file->close();
            }
        }
    }

  QString adressInstallitionRpmfusionNonFree{"dnf install /etc/yum.repos.d/DownloadRpmfusion/RpmFusionNonFree.rpm"};
  installRpmfusionFree(adressInstallitionRpmfusionNonFree);
}

void Dialog::installRpmfusionFree(QString adress)
{
  process.execute(adress);
}
void Dialog::installRpmfusionNonFree(QString adress)
{
  process.execute(adress);
}


// start Button Find Best Link For all repo
void Dialog::on_finBestMirrorForAllButton_clicked()
{
  QUrl adressFedora{"https://admin.fedoraproject.org/mirrormanager/mirrors/Fedora/30/x86_64"};
  downloadWebsitFedoraForButtonFindBest(adressFedora);
}

void Dialog::downloadWebsitFedoraForButtonFindBest(QUrl url)
{
  qDebug() << __FUNCTION__;

  _manager = new QNetworkAccessManager(this);
  connect(_manager , &QNetworkAccessManager::finished , this , &Dialog::findLinksWebsitFedoraForButtonFindBest);
  _URL = url;
  _request.setUrl(_URL);
  _manager->get(_request);
}

void Dialog::findLinksWebsitFedoraForButtonFindBest(QNetworkReply *findLinksFedora)
{
  dataFedora=findLinksFedora->readAll();
  _htmlFile = dataFedora;
  _linksFedora = _htmlFile;
  regexFindLinksFedora();
  _queuFedora = _listLinkFedora;
  readLinksFirstItemOfListFedora();
}


void Dialog::readLinksFirstItemOfListFedora()
{
  qDebug() << __FUNCTION__ << _queuFedora.count() << "Items remained";

  if(_queuFedora.isEmpty())
    {
      qDebug() << __FUNCTION__;
      QString cutTheLinkFedora = _calculateTimeFedora.value(_ntimeCurrent);
      int pos1 =cutTheLinkFedora.lastIndexOf(QString("/releases"));
      _finalBestLinkFedora = cutTheLinkFedora.left(pos1);
      replaceToIniFileForFedora();
    }

  else
    {
      QString downloadFirstItem =  _queuFedora.takeFirst();
      createDynamicLink(downloadFirstItem , "fedora");
    }
}

void Dialog::readLinksFirstItemOfListFedoraFinished()
{
  qDebug() << __FUNCTION__ << _dynamicLinkIniFile << _LinkDynamicFedora;
  QString finishLinkForItems = _dynamicLinkIniFile + _LinkDynamicFedora;

  _time.start();
  downloadItemsFedora(finishLinkForItems);
}

void Dialog::downloadItemsFedora(QUrl url)
{
  qDebug() << __FUNCTION__ << url;

  _manager = new QNetworkAccessManager(this);
  connect(_manager , &QNetworkAccessManager::finished , this , &Dialog::downloadItemsFedoraFinished);
  _request.setRawHeader("Range" , "bytes=" + QByteArray::number(0) + "-" + QByteArray::number(BYTE_DOWNLOAD_));
  _URL = url;
  _request.setUrl(_URL);
  _manager->get(_request);
}

void Dialog::downloadItemsFedoraFinished(QNetworkReply * reply)
{
  qDebug() << __FUNCTION__;
  if(reply->error())
    {
      qDebug() << "ERROR!";
      qDebug() << reply->errorString();
    }

  _ntimeCurrent = _time.elapsed();
  QString finishLinkForItems  = _URL.toString();
  _calculateTimeFedora.insert(_ntimeCurrent , finishLinkForItems);
  qDebug() << __FUNCTION__ << _ntimeCurrent << finishLinkForItems;
  readLinksFirstItemOfListFedora();
}


void Dialog::createDynamicLink(QString dynamic , QString whatis )
{
  QString dynamicLink = dynamic;
  QString isFedoraOrRpmfusion=whatis;

  QSettings settings(ROOT_ETC_YUM_PATH "IniFiles/alghoritmDynamicLink.ini", QSettings::IniFormat);

  settings.beginGroup("dynamicLink");
  _dynamicLinkIniFile = settings.value("alghorithmDynamicLink").toString();
  settings.endGroup();

  _dynamicLinkIniFile.replace("$link" , dynamicLink);
  _dynamicLinkIniFile.replace("$basearch" , _baseArch);
  _dynamicLinkIniFile.replace("$releasever" ,_releaseever);

  downloadDynamicLink(_dynamicLinkIniFile, isFedoraOrRpmfusion);
}

void Dialog::downloadDynamicLink(QString url , QString whatis)
{
  QUrl dynamicUrl = url;
  QString isFedoraOrRpmfusion=whatis;

  qDebug() << __FUNCTION__ << dynamicUrl;

  _manager = new QNetworkAccessManager(this);

  if (isFedoraOrRpmfusion == "fedora")
    {
      connect(_manager , &QNetworkAccessManager::finished , this , &Dialog::downloadFinishedDynamicLinkFedora);
    }
  else if (isFedoraOrRpmfusion == "rpmFusion" )
    {
      connect(_manager , &QNetworkAccessManager::finished , this , &Dialog::downloadFinishedDynamicLinkRpmfusion);
    }

  dynamicUrl = url;
  _request.setUrl(dynamicUrl);
  _manager->get(_request);
}

void Dialog::downloadFinishedDynamicLinkFedora(QNetworkReply *downloadFinishedDynamicLinkFedora)
{
  qDebug() << __FUNCTION__;

  QString dataDynamicLink = downloadFinishedDynamicLinkFedora->readAll();
  QStringList _finishLinkDynamicFedora;

  regexDynamicFedora.indexIn(dataDynamicLink);
  _finishLinkDynamicFedora = regexDynamicFedora.capturedTexts();
  int pos = 0;
  while ((pos = regexDynamicFedora.indexIn(dataDynamicLink, pos)) != -1)
    {
      _finishLinkDynamicFedora << regexDynamicFedora.cap();
      pos += regexDynamicFedora.matchedLength();
    }
  _LinkDynamicFedora = _finishLinkDynamicFedora.takeFirst();

  readLinksFirstItemOfListFedoraFinished();
}

void Dialog::downloadFinishedDynamicLinkRpmfusion(QNetworkReply *downloadFinishedDynamicLinkRpmFusion)
{
  qDebug() << __FUNCTION__;

  QString dataDynamicLink = downloadFinishedDynamicLinkRpmFusion->readAll();
  QStringList _finishLinkDynamicRpm;
  regexDynamicRpmFuison.indexIn(dataDynamicLink);
  _finishLinkDynamicRpm = regexDynamicRpmFuison.capturedTexts();
  int pos = 0;
  while ((pos = regexDynamicRpmFuison.indexIn(_linksRpm, pos)) != -1)
    {
      _finishLinkDynamicRpm << regexDynamicRpmFuison.cap();
      pos += regexDynamicRpmFuison.matchedLength();
    }
  _LinkDynamicRpm = _finishLinkDynamicRpm.takeFirst();

  readLinksFirstItemOfListRpmFuisionFinished();
}


void Dialog::replaceToIniFileForFedora( )
{
  qDebug() << __FUNCTION__;

  QSettings settings(ROOT_ETC_YUM_PATH "IniFiles/alghoritmRepo.ini", QSettings::IniFormat);

  //Alghoritm files fedora
  settings.beginGroup("fedora");
  _aIniFileF = settings.value("alghorithmFedoraMain").toString();
  settings.endGroup();

  _aIniFileF.replace("$link" , _finalBestLinkFedora);
  _aIniFileF.replace("$basearch" , _baseArch);
  _aIniFileF.replace("$releasever" ,_releaseever);

  //Alghoritm files fedora-updates
  settings.beginGroup("fedora-update");
  _aIniFileFU = settings.value("alghorithmFedoraUpdateMin").toString();
  settings.endGroup();

  _aIniFileFU.replace("$link" , _finalBestLinkFedora);
  _aIniFileFU.replace("$basearch" , _baseArch);
  _aIniFileFU.replace("$releasever" ,_releaseever);

  QUrl adressRpmFusion{"https://mirrors.rpmfusion.org/mm/publiclist/"};
  downloadWebsitRpmFusion(adressRpmFusion);

}



void  Dialog::fixSharp(const QString &path)
{
  QFile file(path);
  file.open(QFile::ReadOnly);

  QByteArray data = file.readAll();
  data.replace("%23", "#");
  data.replace("metalink" , "#");

  file.close();
  file.open(QFile::WriteOnly);
  file.write(data);
  file.close();
}

//start RpmFusion

void Dialog::downloadWebsitRpmFusion(QUrl url)
{
  _manager = new QNetworkAccessManager(this);

  connect(_manager , &QNetworkAccessManager::finished , this , &Dialog::findLinksRpm);

  _URL = url;
  _request.setUrl(_URL);
  _manager->get(_request);

}
void Dialog::findLinksRpm(QNetworkReply *findLinks)
{
  QByteArray data;
  data=findLinks->readAll();
  _linksRpm=data;

  //    QRegExp rx("(\\d+)");
  //     QString str = "Offsets: 12 14 99 231 7";
  //     QStringList list;
  //     int pos = 0;

  //     while ((pos = rx.indexIn(str, pos)) != -1) {
  //         list << rx.cap(1);
  //         pos += rx.matchedLength();
  //     }
  //     // list: ["12", "14", "99", "231", "7"]

  int pos = 0;

  while ((pos = regexRpmFusionLinks.indexIn(_linksRpm, pos)) != -1)
    {
      _listLinksRpmFuison << regexRpmFusionLinks.cap(1);
      pos += regexRpmFusionLinks.matchedLength();
    }

  QStringList list = _listLinksRpmFuison;
  list = list.toSet().toList();

  _listLinksRpmFuison = list;

  readLinksFirstItemOfListRpm();
}

void Dialog::readLinksFirstItemOfListRpm()
{
  if(_queuRpm.isEmpty())
    {

      qDebug() << __FUNCTION__;
      QString cutTheLinkRpm = _calculateTimeRpm.value(_ntimeCurrent);
      int pos2 =cutTheLinkRpm.lastIndexOf(QString("/releases"));
      _finalBestLinkRpm = cutTheLinkRpm.left(pos2);
      replaceToIniFileForRpm();

    }

  else
    {
      _time.start();

      QString downloadFirstItem =  _queuRpm.takeFirst();
      createDynamicLink(downloadFirstItem , "rpmFusion");

    }
}

void Dialog::readLinksFirstItemOfListRpmFuisionFinished()
{
  QString finishLinkForItems = _dynamicLinkIniFile + _LinkDynamicRpm;
  _time.start();
  downloadItemsRpmFusion(finishLinkForItems);

}

void Dialog::downloadItemsRpmFusion(QUrl url)
{
  qDebug() << __FUNCTION__ << url;

  _manager = new QNetworkAccessManager(this);
  connect(_manager , &QNetworkAccessManager::finished , this , &Dialog::downloadItemsRpmFusionFinished);
  _request.setRawHeader("Range" , "bytes=" + QByteArray::number(0) + "-" + QByteArray::number(BYTE_DOWNLOAD_));
  _URL = url;
  _request.setUrl(_URL);
  _manager->get(_request);

}

void Dialog::downloadItemsRpmFusionFinished(QNetworkReply *reply)
{
  if(reply->error())
    {
      qDebug() << "ERROR!";
      qDebug() << reply->errorString();
    }

  _ntimeCurrent = _time.elapsed();
  QString finishLinkForItems  = _URL.toString();
  _calculateTimeRpm.insert(_ntimeCurrent , finishLinkForItems);

  qDebug() << __FUNCTION__ << _ntimeCurrent << finishLinkForItems;
  readLinksFirstItemOfListRpm();

}

void Dialog::replaceToIniFileForRpm()
{
  qDebug() << __FUNCTION__;

  QSettings settings(ROOT_ETC_YUM_PATH "IniFiles/alghoritmRepo.ini", QSettings::IniFormat);

  //Alghoritm files rpmfusion-free.repo
  settings.beginGroup("rpmfusion-free");
  _aIniFileRF = settings.value("alghorithmRpmFusion").toString();
  settings.endGroup();

  _aIniFileRF.replace("$basearch" , _baseArch);
  _aIniFileRF.replace("$releasever" ,_releaseever);
  _aIniFileRF.replace("$link" , _finalBestLinkRpm);

  //Alghoritm files rpmfusion-free-updates.repo
  settings.beginGroup("rpmfusion-free-updates");
  _aIniFileRFU = settings.value("alghorithmRpmfusionFreeUpdate").toString();
  settings.endGroup();

  _aIniFileRFU.replace("$basearch" , _baseArch);
  _aIniFileRFU.replace("$releasever" ,_releaseever);
  _aIniFileRFU.replace("$link" , _finalBestLinkRpm);

  //Alghoritm files rpmfusion-nonfree.repo
  settings.beginGroup("rpmfusion-nonfree");
  _aIniFileRFUNonFree = settings.value("alghorithmRpmfusionNonFree").toString();
  settings.endGroup();

  _aIniFileRFUNonFree.replace("$basearch" , _baseArch);
  _aIniFileRFUNonFree.replace("$releasever" ,_releaseever);
  _aIniFileRFUNonFree.replace("$link" , _finalBestLinkRpm);

  //Alghoritm files rpmfusion-nonfree-updates.repo
  settings.beginGroup("rpmfusion-nonfree-updates");
  _aIniFileRFUNonFreeUpdate = settings.value("alghorithmRpmfusionNonFreeUpdate").toString();
  settings.endGroup();

  _aIniFileRFUNonFreeUpdate.replace("$basearch" , _baseArch);
  _aIniFileRFUNonFreeUpdate.replace("$releasever" ,_releaseever);
  _aIniFileRFUNonFreeUpdate.replace("$link" , _finalBestLinkRpm);

  //    QMessageBox Msgbox;

  //    Msgbox.setText("now finde best mirror do you want save now , if you like please clike button save , thanks");
  //    Msgbox.exec();
  QMessageBox::StandardButton buttonSaveOrCancell;

  buttonSaveOrCancell = QMessageBox::question(this, "Save", "Now finde best mirror do you want save ?",
                                              QMessageBox::Save  | QMessageBox::Cancel);

  if (buttonSaveOrCancell == QMessageBox::Save)
    {
      writeToFileFedora(ROOT_ETC_YUM_PATH);
      writeToFileRpmFusion(ROOT_ETC_YUM_PATH);
    }

  if(buttonSaveOrCancell == QMessageBox::Cancel)
    {
      close();
    }

}

void Dialog::on_saveButton_clicked()
{

}

void Dialog::writeToFileFedora(QString addressFile)
{
  QDir directory(addressFile);

  QStringList repoFiles = directory.entryList(QStringList() << "*.repo" ,QDir::Files);

  for (int i = 0 ; i <= repoFiles.size(); i++)
    {
      if(repoFiles.at(i) == "fedora.repo")
        {
          QString file;
          file = directory.path();
          file.append("/");
          file.append(repoFiles.at(i));
          QSettings *settings;
          settings = new QSettings(file , QSettings::NativeFormat);
          settings->beginGroup("fedora");
          settings->setValue("baseurl" ,_aIniFileF );
          settings->endGroup();

          delete settings;
          fixSharp(file);
        }

      else if(repoFiles.at(i) == "fedora-update.repo")
        {
          QString file;
          file = directory.path();
          file.append("/");
          file.append(repoFiles.at(i));
          QSettings *settings;
          settings = new QSettings (file , QSettings::NativeFormat);
          settings->beginGroup("updates");
          settings->setValue("baseurl" ,_aIniFileFU );
          settings->endGroup();

          delete  settings;
          fixSharp(file);
        }
    }
}

void Dialog::writeToFileRpmFusion(QString addressFile)
{
  QDir directory(addressFile);
  QStringList repoFiles = directory.entryList(QStringList() << "*.repo" ,QDir::Files);

  for (int i = 0 ; i <= repoFiles.size(); i++)
    {

      if (repoFiles.at(i) == "rpmfusion-free.repo")
        {
          QString file;
          file = directory.path();
          file.append("/");
          file.append(repoFiles.at(i));
          QSettings * settings;
          settings  = new QSettings(file , QSettings::NativeFormat);
          settings->beginGroup("rpmfusion-free");
          settings->setValue("baseurl" ,_aIniFileRF );
          settings->endGroup();
          delete   settings;

          fixSharp(file);

        }

      else if (repoFiles.at(i) == "rpmfusion-free-updates.repo")
        {
          QString file;
          file = directory.path();
          file.append("/");
          file.append(repoFiles.at(i));
          QSettings *settings;
          settings = new QSettings(file , QSettings::NativeFormat);

          settings->beginGroup("rpmfusion-free-updates");
          settings->setValue("baseurl" ,_aIniFileRFU );
          settings->endGroup();
          delete settings;
          fixSharp(file);
        }

      else if (repoFiles.at(i) == "rpmfusion-nonfree.repo")
        {
          QString file;
          file = directory.path();
          file.append("/");
          file.append(repoFiles.at(i));
          QSettings * settings;
          settings = new QSettings(file , QSettings::NativeFormat);
          settings->beginGroup("rpmfusion-nonfree");
          settings->setValue("baseurl" ,_aIniFileRFUNonFree );
          settings->endGroup();
          delete  settings;
          fixSharp(file);
        }
      else if (repoFiles.at(i) == "rpmfusion-nonfree-updates.repo")
        {
          QString file;
          file = directory.path();
          file.append("/");
          file.append(repoFiles.at(i));
          QSettings * settings;
          settings = new  QSettings(file , QSettings::NativeFormat);
          settings->beginGroup("rpmfusion-nonfree-updates");
          settings->setValue("baseurl" ,_aIniFileRFUNonFreeUpdate );
          settings->endGroup();

          delete settings;
          fixSharp(file);
        }
    }
}
