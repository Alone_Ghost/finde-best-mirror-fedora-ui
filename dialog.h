#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QString>
#include <QDir>
#include <QList>
#include <QMap>
#include <QTime>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrl>
#include <QRegExp>
#include <QSysInfo>
#include <QSettings>
#include <QProcess>
#include <QHBoxLayout>
#include <QTreeWidget>
#include <QProcess>
#include <QMessageBox>
#include <QtXml>

namespace Ui
{
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();

private slots:

    void on_runButton_clicked();

    void on_exitButton_clicked();

    void on_addedRepostioryRpmFusion_clicked();

    void on_addedRpmfusionFreeButton_clicked();

    void on_addedBothRepoRpmfusionButton_clicked();

    void on_addedRpmFusionNonFreeButton_clicked();

    void on_finBestMirrorForAllButton_clicked();

    void on_saveButton_clicked();

public slots:
    //create download and find links , country
    void downloadWebsitFedora(QUrl url);
    void findLinksFedora(QNetworkReply *findLinksFedora);
    void downloadWebsitRpmfusion(QUrl url);
    void findLinksRpmFusion(QNetworkReply *findLinksRpmfusion);

    //Download Rpmfusion Free Repository
    void downloadRpmfusionFree();
    void writeRpmFusionFree(QNetworkReply *wirteThatFile);

    //Download Rpmfusion Non-Free Repository
    void downloadRpmfusionNonFree();
    void writeRpmFusionNonFree(QNetworkReply *wirteThatFile);

    //treewidget
    void addRootTreeWidget(QString nameTree , QString nameChild , QString forWhat);
    void addChildTreeWidget( QTreeWidgetItem *parent, QString link );

    //create ini file and set value
    void findInfoSystem();
    void createPatchForIni();

    //create Download Link RpmFusion Free and Non-Free
    void createLinkForDownloadRpmfusionFree();
    void createLinkForDownloadRpmfusionNonFree();

    void installRpmfusionFree(QString adress);
    void installRpmfusionNonFree(QString adress);

    //function Run Regex For find links
    void regexFindLinksFedora();

    void downloadWebsitFedoraForButtonFindBest(QUrl url);
    void findLinksWebsitFedoraForButtonFindBest(QNetworkReply *findLinksFedora);

    void downloadWebsitRpmFusionForButtonFindBest(QUrl url);
    void findLinksWebsitRpmFusionForButtonFindBest(QNetworkReply *findLinksFedora);

    // all this function for button Find best mirror
    //function for fedora
    void readLinksFirstItemOfListFedora();
    void createDynamicLink(QString dynamic , QString whatis );
    void downloadDynamicLink(QString url , QString whatis);
    void downloadFinishedDynamicLinkFedora(QNetworkReply *downloadFinishedDynamicLinkFedora);
    void readLinksFirstItemOfListFedoraFinished();
    void downloadItemsFedora(QUrl url);
    void downloadItemsFedoraFinished(QNetworkReply * reply);
    void replaceToIniFileForFedora();
    void writeToFileFedora(QString addressFile);

    //function for RpmFusion
    void downloadWebsitRpmFusion(QUrl url);
    void findLinksRpm(QNetworkReply *findLinks);
    void readLinksFirstItemOfListRpm();
    void downloadFinishedDynamicLinkRpmfusion(QNetworkReply *downloadFinishedDynamicLinkRpmFusion);
    void readLinksFirstItemOfListRpmFuisionFinished();
    void downloadItemsRpmFusion(QUrl url);
    void downloadItemsRpmFusionFinished(QNetworkReply *reply);
    void replaceToIniFileForRpm();
    void writeToFileRpmFusion(QString addressFile);

    //conver %23 to sharp(#) and convert all metalink to #matalink
    void  fixSharp(const QString &path);



private:
    Ui::Dialog *ui;

    //Network
    QNetworkRequest _request;
    QNetworkAccessManager * _manager;

    //List
    QList<QString>_queuRpm;
    QList<QString>_queuFedora;

    //map
    QMap<int,QString>_calculateTimeFedora;
    QMap<int,QString>_calculateTimeRpm;

    QMap<QStringList , QStringList>_listLinkFedoraConcomitantNameCountry; //list link and country name of website fedora
    QMap<QString , QStringList>_listLinkRpmfusionConcomitantNameCountry; //list link and country name of website fedora

    //process for install rpmfusion
    QProcess process;
    //data
    QUrl _URL;
    QTime _time;
    int _ntimeCurrent;
    QString _nmyLinks;
    QFile _openFile;

    QByteArray dataFedora;
    QByteArray dataRpmfusion;

    QString _linksRpm;
    QString _linksRpmFusion;
    QString _linksFedora;
    QString _htmlFile;
    QString _htmlFileRpmfusion;


    QStringList _listLinksRpmFuison;
    QStringList _listLinkFedora;
    QStringList _findListLinkFedora;

    QStringList _findListOfSiteNameCountryFedora;
    QStringList _findListOfListNameCountryFedora;

    QStringList _findListOfSiteNameCountryRpmFuisn;
    QStringList _findListOfListNameCountryRpmFusion;


    QString _LinkDynamicFedora;
    QString _LinkDynamicRpm;

    QString _finalBestLinkFedora;
    QString _finalBestLinkRpm;
    QSysInfo productVersion;
    QSysInfo currentCpuArchitecture;

    QString _releaseever;
    QString _baseArch;


    QString _aIniFileF; //Fedora
    QString _aIniFileFU; //fedora-update
    QString _aIniFileRF; //rpmfusion-free
    QString _aIniFileRFU; //rpmfusion-free-update
    QString _aIniFileRFUNonFree;//rpmfusion-Non-Free
    QString _aIniFileRFUNonFreeUpdate;//rpmfusion-Non-Free-Update
    QString _dynamicLinkIniFile; //create link for Download

    QString _aIniFileRpmfusionFree;
    QString _aIniFileRpmfusionNonFree;

    //regex links
    QRegExp regexRpmFusionLinks{"http(:?s|)://(\bftp-stud|ftp.tu|fr2|mir01|download1|www.mirrorservice|www.fedora|kartolo|muug|mirror|mirrors|repo|ucmirror|ftp|fedora|rpmfusion|)\\.\\w*(:?\\.|)(:?-|)(:?/|)\\w*(:?/|)(:?\\.|)\\w*(:?/|)(:?\\.|)(:?\\w*|)(:?/|)(:?\\.|)(:?\\w*|)(:?/|)(:?\\w*|)(:?\\.|)(:?/|)(:?\\w*|)(:?\\w*|)(\b/free/fedora|/RPMFUSION_free_Fedora|)"};
    QRegExp regexFedoraLinks{"http(:?s|)://(\bfedora-mirror02|download-ib01|pubmirror1|pubmirror2|fr2|mirror2|mirror1|fedora-mirror|distrib-coffee|ftp-stud|www.nic|www.fedora|www.ftp|kambing.ui|www.mirrorservice|espejito|spout|sjc|repo|mirror|ftp|syd|mirrors|lon|hkg|kartolo|nrt|my|ams|ucmirror|sg|linus|dfw|ewr|iad|ord|fedora)(:?\\.|-)(:?\\w*)(:?/|)(:?\\w*|)(:?/|)(:?\\w*|)(:?\\.|)(:?-|)(:?/|)(:?\\w*|)(:?/|)(:?\\w*)(:?\\.|/:?-|)(:?/|\\w*)(:?\\w*|/)(:?/|\\.:?-|)(:?\\w*|)(:?\\.|/:?-|)(:?/|-:?\\.|)(:?\\w*|-)(:?-|/)(:?\\w*)(:?/|)(:?-|)(:?\\w*|)(:?/|\\.:?-|)(:?-|\\w*:?\\.|)(:?\\w*|/)(:?/|)(:?\\w*|\\.)(:?/|)(:?\\.|)(:?\\w*|)(:?/|-:?-|)(:?\\w*|\\.)(:?-|/:?\\.|)(:?\\w*|/:?\\.|)(:?/|)(:?\\w*|\\.)(:?/|)(:?\\.|\\w*)(:?\\w*|/)(:?\\w*|/)(:?-|)(:?/|\\w*)(:?/|\\w*)(:?/|\\w*)(:?\\w*|/)(:?/|)(:?\\w*|)(:?/|)"};

    //regex find name country
    //QRegExp regexExtractionOfSiteNameCountryFedora{"(\bAU|BD|BG|BR|BY|CA|CH|CN|CO|CR|CY|CZ|DE|DK|EC|ES|FI|FR|GB|GR|HK|ID|IL|IS|IT|JP|KE|KR|KZ|LT|LU|MK|MY|NC|NL|NO|NZ|PH|PK|PL|PT|RO|RS|RU|SE|SG|TH|TR|TW|UA|US|UY|ZA)"}; //extraction of website fedora mirror list
   // QRegExp regexExtractionOfSiteNameCountryFedora{"<td>\\w\\w</td>"}; //extraction of website fedora mirror list
 QRegExp regexExtractionOfSiteNameCountryFedora{"(<tr>)(.*)(</tr>)"}; //extraction of variable QStringList(?<=<td>)\w\w(?=</td>)

//"<FONT class=\"result\">(.*)</FONT>"1
// ("<FONT class=\"result\">(.*)</FONT>");                                                                                                                li>.*\((Pg_1[^)]*|TEMPLATE_LOGO).*?<\/li>
    QRegExp regexExtractionOfSiteNameCountryRpmfusion{"(bBG|CA|CN|CZ|DE|DK|EC|FR|GB|GE|GR|ID|IN|IS|MD|MK|NL|NO|NZ|PK|PL|RO|RU|SK|TN|TW|UA|US|VN)"}; //extraction of website fedora mirror list
    //    QRegExp regexExtractionOfSiteNameCountryRpmfusion{"</a>\\.</td>"}; //extraction of website fedora mirror list

    //    QRegExp regexExtractionOfListNameCountryRpmfusion{"(\bBG|CA|CN|CZ|DE|DK|EC|FR|GB|GE|GR|ID|IN|IS|MD|MK|NL|NO|NZ|PK|PL|RO|RU|SK|TN|TW|UA|US|VN)"}; //extraction of variable QStringList


    QRegExp regexDynamicFedora{"astromenace(:?-|)(:?\\d|\\d\\d)(:?\\.|)(:?\\d|\\d\\d)(:?\\.|)(:?\\d|\\d\\d)(:?-|)(:?\\d|\\d\\d)\\.\\w*\\.x86_64.rpm"};
    QRegExp regexDynamicRpmFuison{"aegisub(:?-|)(:?\\d|\\d\\d)(:?\\.|)(:?\\d|\\d\\d)(:?\\.|)(:?\\d|\\d\\d)(:?-|)(:?\\d\\d|\\d\\d\\d)(:?\\.)\\w*\\.x86_64.rpm"};
};
#endif // DIALOG_H
