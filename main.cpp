#include "dialog.h"
#include <QApplication>
#include <QLoggingCategory>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QLoggingCategory::setFilterRules("*.debug=true");

    Dialog w;
//    w.findInfoSystem();
//    w.createPatchForIni();

    w.show();

    return a.exec();
}
